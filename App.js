/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const origin = {latitude: -6.226977,longitude: 106.8334693};
const destination = {latitude: -6.193984,longitude: 106.848378};
const GOOGLE_MAPS_APIKEY = 'AIzaSyAKz6HGlLBCFgYOzHMVuP1OJkSRTgHTw2M';

const data = {
       source: {
        latitude: -33.8356372,
        longitude: 18.6947617
      },
      destination: {
        latitude: -33.8600024,
        longitude: 18.697459
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode 
        }
      ]
    }

type Props = {};
export default class App extends Component<Props> {


  render() {
    return (
       /*<MapView
       style={styles.mapStyle}
        initialRegion={{
          latitude: 5.540202,
          longitude: 95.309818,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }} />*/
       
      /* <View style={styles.container}>

        <MapView style={styles.map}
          region={{
              latitude: -6.226977,
              longitude: 106.8334693,
              latitudeDelta: 0.1,
              longitudeDelta: 0.1
            }}
        >

          <MapView.Marker
            coordinate={{
              latitude: -6.226977,
              longitude: 106.8334693
            }}
            title={'Mandiri Inhealth'}
            description={'Menara Palma Lantai 20'}
          />

        </MapView>
        
       </View>*/
        <View style={styles.container}>
               <MapView style={styles.map} initialRegion={{
                  latitude: -6.226977,
                  longitude: 106.8334693,
                  latitudeDelta: 0.1,
                  longitudeDelta: 0.1
                }}>
                    <MapView.Marker coordinate={origin}
                      title={'Mandiri Inhealth'}
                      description={'Menara Palma Lantai 20'} 
                     />

                  <MapView.Marker coordinate={destination}
                      title={'Pusilkom UI'}
                      description={'Universitas Indonesia'} 
                     />
                  </MapView>
              
            </View>

    );
  }
}

const styles = StyleSheet.create({

                   /* <MapViewDirections
                      origin={origin}
                      destination={destination}
                      apikey={GOOGLE_MAPS_APIKEY}
                    />*/


  /*container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  mapStyle: {
   flex: 1
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },*/

 container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'flex-end',
    alignItems: 'center' 
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }

/*container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    text: {
        fontSize: 30,
        fontWeight: '700',
        color: '#59656C',
        marginBottom: 10,
    },
    map: {
        width: null,
        height: 300,
        flex: 1
    }*/
});
